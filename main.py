import numpy as np
import matplotlib.pyplot as plt
import random

C = 0
S = 0
iteration = 1000000

r = 1


for i in range(iteration):
    x = random.uniform(-r, r)
    y = random.uniform(-r, r)
    if x**2 + y**2 <= r**2:
        S += 1
        C += 1
    else:
        S += 1
    PI = (4*C)/S 
    print('S: ', S, 'C: ', C, 'PI: ', PI)
    

